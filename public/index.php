<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<section id="info" class="center">
    <?php if ($_SERVER['REQUEST_METHOD'] == 'POST') : ?>
        <?php 
            $user_name = sanitize($_POST["name"]); 
            $pw_hash = md5($_POST["password_1"]);
            $success = insertData($user_name,$pw_hash);
        ?>
        <div id="new_user_info">
            <?php if ($success) : ?>
                Vielen Dank zur Registration von <?php echo $user_name; ?><br>
                <div id="timestamp"></div>
            <?php else: ?>
                <div class="error">
                    Registration of <?php echo $user_name; ?> failed <br>
                </div>
            <?php endif ?>
        </div>
    <?php endif ?>
</section>
<section id="main_form" class="center">
    <form action="index.php" method="post">
        Name: <input type="text" name="name" id="name"><br>
        Password: <input type="password" name="password_1" id="password_1" ><br>
        Password bestätigen: <input type="password" name="password_2" id="password_2" oninput="checkPasswords()"><br>
    <input type="submit" disabled id="send_button">
    <p id="client_error"></p>
    </form>
</section>
</body>
</html>




<?php
function insertData($user_name, $pw_hash) : bool{
    //Database management as in https://www.w3schools.com/php/php_mysql_insert.asp

    $servername = "localhost";
    $username = "admin";
    $password = "thi$ 1s an actually hard passowrd, in'it?";
    $dbname = "a2_users";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        return false;
    }

    $sql = "INSERT INTO `user` (username, password_hash)
    VALUES ('$user_name', '$pw_hash')";

    $success = $conn->query($sql);

    if (! $success){
        echo mysqli_error($conn);
    }

    $conn->close();

    return $success;
}
?>

<?php
function sanitize($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
?>



<script>
    async function writeTime(){
        let res = await fetch("http://worldclockapi.com/api/json/cet/now");
        let dat = await res.json()
        timeElem.innerHTML = dat.currentDateTime
    }
    let timeElem=document.getElementById('timestamp');
    if (timeElem !== undefined){
        writeTime();
    }

    function checkPasswords(){
        let p1 = document.getElementById('password_1').value
        let p2 = document.getElementById('password_2').value
        let errElem = document.getElementById('client_error')
        let sendElem = document.getElementById('send_button')
        if (p1 == p2){
            errElem.innerHTML = "  "
            sendElem.disabled=false
        }else{
            errElem.innerHTML = " Password Missmatch "
            sendElem.disabled=true
        }

        return p1==p2
    }

</script>


<link href="https://fonts.googleapis.com/css?family=Quicksand:300,500" rel="stylesheet">
<style>
    body{
        background-color: darkslategray;
        font-family: 'Quicksand', sans-serif;
        font-weight: 400;
    }

    .center {
        margin: auto;
        width: 50%;
    }

    #main_form{
        background-color: white;
        border-radius: 2em;
        border: 1px solid grey;
        padding: 5em;
        margin-top: 2em;
        box-shadow: black;
    }

    #client_error{
        color:red
    }

    input[type=text],input[type=password], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    }
    input[type=submit]:hover {
    background-color: #45a049;
    }
    input[type=submit]:disabled {
    background-color: lightgray;
    }

    div {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
    }

</style>
