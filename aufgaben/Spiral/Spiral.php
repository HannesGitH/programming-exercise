<?php

declare(strict_types=1);

namespace Aufgaben\Spiral;

/**
 * Erzeuge eine Spiralmatrix mit der Größe length * length.
 *
 * @method static array make(int $length)
 * @example Spiral::make(3) === [
 *     [1, 2, 3],
 *     [8, 9, 4],
 *     [7, 6, 5]]
 */
final class Spiral
{
    //uh okay thats not as trivial as the first ones
    // there might be a more elegant solution that is not in O(n^2), but this is how a human would probably draw it
    static function make(int $length) : array {

        $highest_number = $length*$length;
        $array = array_fill_keys(range(0,$length-1), range(0,$length-1)); //wow i just want to init an empty matrix, thats why PHP isnt most peoples favorite
        $already_visited = array_fill_keys(range(0,$length-1), array_fill_keys(range(0,$length-1), false)); //same here
        $direction = 0;
        $x = 0; $y = 0;

        for ($current_number = 1; $current_number <= $highest_number;){
            if($x < $length && $y < $length && $x >=0 && $y >=0  &&!$already_visited[$x][$y]){ //check whether the index is out of bounds and if it isnt whether we already set it
                $array[$y][$x] = $current_number; // set spiral number (swapped x and y because the spiral should be inner first)
                $already_visited[$x][$y] = true;  // we have now visited this index
                $current_number++;
            }else{
                //we've been here before, undo last walking step
                switch ($direction) {
                    case 0:
                        $x--;
                        break;
                    case 1:
                        $y--;
                        break;
                    case 2:
                        $x++;
                        break;
                    case 3:
                        $y++;
                        break;
                    
                    default:
                        //throw new Exception('Yo, there is only four directions in a 2D Spiral.');
                        break;
                }  
                // turn
                $direction=($direction+1)%4;
            } 
            Spiral::walk($direction,$x,$y);   // walk to the next index (if the spiral class should be extended, one would make $x and $y class vars)
        }
        return $array;
    }

    static function walk(&$direction, &$x, &$y){
        switch ($direction) {
            case 0:
                $x++;
                break;
            case 1:
                $y++;
                break;
            case 2:
                $x--;
                break;
            case 3:
                $y--;
                break;
            
            default:
                //throw new Exception('Yo, there is only four directions in a 2D Spiral.');
                break;
        }  
    }
}

