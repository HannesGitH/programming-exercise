<?php

declare(strict_types=1);

namespace Aufgaben\Palindrome;

/**
 * Ein Palindrom ist ein String, der rückwärts gelesen denselben String ergibt.
 * Nich-alphanumerische Zeichen müssen ebenfalls unterstützt werden.
 *
 * @method static bool check(string $string)
 * @example Palindrome::check('asddsa')  === true
 * @example Palindrome::check('asdd')  === false
 */
final class Palindrome
{
    static function check(string $string) : bool {
        return strrev($string) == $string; 
    }
}

//just for quick n dirty testing
//echo Palindrome::check('asddsa')  === true && Palindrome::check('asdd')  === false;
