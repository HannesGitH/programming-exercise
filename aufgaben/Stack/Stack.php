<?php

declare(strict_types=1);

namespace Aufgaben\Stack;

/**
 * Erzeuge einen Stack.
 *
 * Wenn der Stack leer ist sollen die pop- und peek-Methiden null zurückgeben.
 *
 * @method void push(mixed $value)
 * @method mixed|null pop()
 * @method mixed|null peek()
 * @example $queue = new Stack();
 * $queue->push(1);
 * $queue->push(2);
 * $queue->peek() === 1;
 * $queue->pop() === 1;
 */
final class Stack
{
    /**
     * okay the description wants a FIFO Stack but the UnitTests test for LIFO stack
     * 
     * so just set the one you want
     * 
     * @var bool
     */
    public $isFIFO = false;

    /**
     * represents the stack (can be of any type)
     * @var array
     */
    private $_stack = [];

    
    /**
     * @method push : pushes a new value onto the stack
     * @param mixed $value
     * 
     * @return void
     */
    public function push(mixed $value)
    {
        array_push($this->_stack, $value);
    }

    /**
     * @method pop : returns and removes next awaiting elem
     * @return mixed
     */
    public function pop() : mixed
    {
        if ($this->isFIFO){
            return array_shift($this->_stack);
        }
        return array_pop($this->_stack);
    }

    /**
     * @method peek : returns the next awaiting elem without removing it
     * @return mixed
     */
    public function peek() : mixed
    {
        if ($this->isFIFO){
            return $this->_stack[0];
        }else{
            // this makes me miss python
            $elem = $this->pop();
            $this->push($elem);
            return $elem;
        }
    }
}
