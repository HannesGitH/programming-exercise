<?php

declare(strict_types=1);

namespace Aufgaben\Reverse;

/**
 * Erstelle Methoden, die den gegebenen Input umkehren.
 *
 * @method static int int(int $number)
 * @method static string string(string $string)
 * @example Reverse::int(12) === 21
 * @example Reverse::int(912) === 219
 * @example Reverse::int(120) === 21
 * @example Reverse::int(-12) === -21
 * @example Reverse::int(-120) === -21
 * @example Reverse::string('qwerty')  === 'ytrewq'
 * @example Reverse::string('apple')  === 'elppa'
 */
final class Reverse
{
    static function int(int $number) : int{
        $reversed_number = 0;
        while ($number != 0){
            $next_digit = $number % 10;
            $reversed_number = $reversed_number *10 + $next_digit;
            $number = ($number - $next_digit) / 10;
        }
        return $reversed_number;
    }

    static function string(string $string) : string{
        // if this is cheating just tell me
        return strrev($string);
    }
}

