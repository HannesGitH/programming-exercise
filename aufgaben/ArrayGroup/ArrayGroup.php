<?php

declare(strict_types=1);

namespace Aufgaben\ArrayGroup;

/**
 * Zerteile das Array in mehrere Teil-Arrays, so dass
 * so viele Teil-Array wie möglich die angegebene Größe besitzen.
 *
 * @method static any[] group(any[] $array, int $size)
 * @example ArrayGroup::group([1, 2, 3, 4, 5], 2) -> [[ 1, 2], [3, 4], [5]]
 * @example ArrayGroup::group([1, 2, 3, 4, 5], 3) -> [[ 1, 2, 3], [4, 5]]
 * @example ArrayGroup::group([1, 2, 3, 4, 5], 6) -> [[ 1, 2, 3, 4, 5]]
 */
final class ArrayGroup
{
    static function group(array $array, int $size) : array{
        // is this cheating?
        return array_chunk($array , $size);
    }
}
//just for quick n dirty testing
//print_r( ArrayGroup::group([1, 2, 3, 4, 5], 3) );
